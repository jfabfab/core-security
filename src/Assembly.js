const appConfig = require('config');
const express = require('express');
const fs = require('fs');
const http = require('http');
const handlebars = require('handlebars');
const swaggerExpressMiddleware = require('swagger-express-middleware');
const pkg = require('../package.json');
const YAML = require('yamljs');
const util = require('util');
const winston = require('winston');
const { Monitoring } = require('./Monitoring');
const { Router } = require('./Router');

const core = require('./core');

const { Tokenizer } = require('./tokenizer/Tokenizer');
const { IssuesReporterClient } = require('issues-reporter-client');
const { JsonWebToken } = require('./jsonWebToken/JsonWebToken');

const readFile = util.promisify(fs.readFile);
const createSwaggerMiddleware = util.promisify(swaggerExpressMiddleware);

/**
 * This class is responsible to build all the application common dependencies
 *
 * @class Assembly
 */
class Assembly {
  /**
   * Initializes all the shared instances.
   *
   * @returns
   *
   * @memberof Assembly
   */
  async init(/* istanbul ignore next: default values */ {
    externalConfigPath = '/run/config/project_folder/config.json' } = {}
  ) {
    // core
    // this.assembly = this;

    this.logger = await this._initLogger();

    this._initGlobal();

    // configs
    this.config = await this._initConfig({ externalConfigPath });
    this.swaggerConfig = await this._initSwaggerConfig();

    // service
    const serviceItems = await this._initService();
    this.serviceDriver = serviceItems.serviceDriver;
    this.httpServer = serviceItems.httpServer;
    this.swaggerMiddleware = serviceItems.swaggerMiddleware;

    // app instances
    await this._initAppInstances();

    // should be initialized last because it usually needs all the dependencies
    this.monitoring = new Monitoring(this);
    this.router = new Router(this);
  }

  /**
   * This is where you would create your app instances.
   *
   * @memberof Assembly
   */
  async _initAppInstances() {
    // NOTE: Creation order is important.
    const privateKey = await this._getPrivateKey();
    this.jsonWebToken = new JsonWebToken({ privateKey });

    this.tokenizer = new Tokenizer(this);

    let reporterEndpoint = '';
    if (this.config.routes && this.config.routes.apigateway) {
      reporterEndpoint = `${this.config.routes.apigateway}/issues`;
    }

    this.reporter = new IssuesReporterClient({
      endpoint: reporterEndpoint,
      serviceName: this.config.service.name,
      serviceVersion: this.config.service.version,
      retryCount: 2,
      logger: this.logger,
    });
  }

  /**
   * Insert some items that available globally
   * NOTE: Add only lightweight helper items, global should be avoided for business logic classes
   *
   * @memberof Assembly
   */
  _initGlobal() {
    global.assembly = this;
  }

  /**
   * Create the config objects. The config object consists of the local config `/config/*`
   * in addition to the console config specified in the cie console https://console-develop.score-securityloud.io/manage
   * The console config will override existing configs if they exist in the local configs.
   *
   * @param {any} { externalConfigPath }
   * @returns
   * @memberof Assembly
   */
  async _initConfig({ externalConfigPath }) {
    let config = await this._buildConfig({ externalConfigPath });

    // inject package.json
    config = appConfig.util.extendDeep(config, { service: pkg });

    delete config.service.dependencies;
    delete config.service.devDependencies;
    delete config.service.scripts;

    // service
    let host = config.service.coreSecurity.host;
    if (config.apigateway && config.apigateway.host) {
      host = config.apigateway.host;
    }

    config.service.host = host.replace(/(^\w+:|^)\/\//, '');
    config.service.basePath = `/${config.service.coreSecurity.serviceGroup}/v${config.service.version.split('.')[0]}/${config.service.name}`;
    config.service.url = `https://${config.service.host}${config.service.basePath}`;

    return config;
  }

  /**
   * Builds the config object from node-config merged with external configs
   *
   * @param {any} { externalConfigPath }
   * @returns
   * @memberof Assembly
   */
  async _buildConfig({ externalConfigPath }) {
    if (!externalConfigPath) {
      this.logger.info('No external config was injected.');
      return appConfig;
    }

    try {
      const configBuffer = await readFile(externalConfigPath);
      return appConfig.util.extendDeep(appConfig, JSON.parse(configBuffer));
    } catch (err) {
      /* istanbul ignore else: global throw err */
      if (err && err.code === 'ENOENT') {
        this.logger.info('No external config was injected (file doesn\'t exist).');
        return appConfig;
      }

      /* istanbul ignore next: global throw err */
      throw err;
    }
  }

  /**
   * read RSA private key from config
   *
   * @returns
   * @memberof Assembly
   */
  async _getPrivateKey() {
    try {
      const privateKey = await readFile(this.config.paths.privateKey);
      return privateKey;
    } catch (err) {
      /* istanbul ignore else: global throw err */
      if (err && err.code === 'ENOENT') {
        this.logger.error('No private key was injected (file doesn\'t exist).');
      }

      /* istanbul ignore next: global throw err */
      throw new Error(err);
    }
  }

  /**
   * Create the logger object
   *
   * @returns
   * @memberof Assembly
   */
  async _initLogger() {
    /* istanbul ignore next: logger format */
    const customedFormat = data => `${data.timestamp} [${data.level}]: ${data.message}`;

    /* istanbul ignore next: logger initialization */
    const logger = winston.createLogger({
      format: winston.format.combine(
        winston.format.colorize(),
        winston.format.timestamp(),
        winston.format.printf(customedFormat),
      ),
      transports: [
        // levels => silly: 5, debug: 4, verbose: 3, info: 2, warn: 1, error: 0
        new winston.transports.Console({ level: 'silly' }),
      ],
      exceptionHandlers: [
        new winston.transports.File({ filename: 'exceptions.log' })
      ]
    });

    return logger;
  }

  /**
   * Create the swagger config objects. This configuration is used by the swagger middleware
   * and by the router to return the swagger file.
   *
   * @returns
   * @memberof Assembly
   */
  async _initSwaggerConfig() {
    const swaggerResolver = new core.resolvers.Swagger(this.logger);
    const buffer = await swaggerResolver.resolveSwaggerFile('swagger/index.yaml');
    const template = await handlebars.compile(JSON.stringify(buffer));
    const jsonString = await template({ package: this.config.service });
    const jsonConfig = JSON.parse(jsonString);
    const result = await YAML.stringify(jsonConfig, 100);
    return result;
  }

  /**
   * Create the service driver objects (and their dependencies). The default driver is
   * express.
   *
   * @returns
   * @memberof Assembly
   */
  async _initService() {
    const serviceDriver = express();
    const httpServer = http.Server(serviceDriver);
    const swaggerMiddleware = await createSwaggerMiddleware(
      YAML.parse(this.swaggerConfig),
      serviceDriver
    );

    return {
      httpServer,
      serviceDriver,
      swaggerMiddleware,
    };
  }


  /**
   * Cleans the instances. When the app stops, this method is called. We can put db
   * connections disconnection for example.
   *
   * @memberof Assembly
   */
  async clean() {
    // NOTE: This is where you put your cleaning before the process exits
  }
}

module.exports = {
  Assembly,
};
