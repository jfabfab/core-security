/* global loggerForTests wait sinon expect mocks fixtures */

const pkg = require('../package.json');
const proxyquire = require('proxyquire');

describe('Test suite for Router', () => {
  let Router;
  let routerArgs;

  beforeEach(() => {
    const monitoringMock = {
      getCurrentCounters: sinon.stub().returns({ counters: 'test' }),
    };

    const coreMock = {
      middlewares: {
        headerVersion: sinon.stub(),
        responseFormatter: sinon.stub(),
        protectJSON: sinon.stub(),
      },
    };

    const tokenizerMock = {
      generate: sinon.stub().returns('test-token')
    };

    const reporterMock = {
      reportIssue: sinon.stub()
    };

    routerArgs = {
      logger: loggerForTests,
      config: fixtures.config,
      monitoring: monitoringMock,
      serviceDriver: new mocks.express.AppMock(),
      swaggerConfig: 'test swagger',
      swaggerMiddleware: new mocks.swagger.MiddlewareMock(),
      tokenizer: tokenizerMock,
      reporter: reporterMock,
    };

    Router = proxyquire('./Router', {
      './core': coreMock,
    }).Router;
  });

  it('should register endpoints', () => {
    // prepare
    const router = new Router(routerArgs);

    // run
    function run() {
      router.registerRoutes();
    }

    // result
    function test() {
      expect(routerArgs.swaggerMiddleware.metadata).to.have.been.called();
      expect(routerArgs.swaggerMiddleware.CORS).to.have.been.called();
      expect(routerArgs.swaggerMiddleware.parseRequest).to.have.been.called();
      expect(routerArgs.swaggerMiddleware.validateRequest).to.have.been.called();

      expect(routerArgs.serviceDriver.get).to.have.been.calledWith('/');
      expect(routerArgs.serviceDriver.get).to.have.been.calledWith('/health');
      expect(routerArgs.serviceDriver.post).to.have.been.calledWith('/token/generate');
      expect(routerArgs.serviceDriver.use).to.have.been.calledWith('/swagger.yaml');
    }

    run();
    test();
  });

  describe('controller', () => {
    let reqMock;
    let resMock;

    beforeEach(() => {
      reqMock = new mocks.express.RequestMock();
      resMock = new mocks.express.ResponseMock();
    });

    async function entry({ method, endpoint, test }) {
      // prepare
      const router = new Router(routerArgs);
      function prepare() {
        router.registerRoutes();
      }

      // run
      function run() {
        routerArgs.serviceDriver.simulateCall(method, endpoint, reqMock, resMock);
      }

      // result
      prepare();
      run();
      await wait();
      test();
    }

    it('should call homeController', async () => entry({
      method: 'get',
      endpoint: '/',
      test: () => {
        expect(resMock.send).to.have.been.calledWith(`<html><body>${pkg.name} v${pkg.version}</body></html>`);
      },
    }));

    it('should call healthController', async () => entry({
      method: 'get',
      endpoint: '/health',
      test: () => {
        expect(resMock.json).to.have.been.calledWith({ status: 200, monitoring: { counters: 'test' } });
      },
    }));

    it('should call swagger.yaml', async () => entry({
      method: 'get',
      endpoint: '/swagger.yaml',
      test: () => {
        expect(resMock.send).to.have.been.calledWith('test swagger');
      },
    }));

    it('should call authenticate', async () => entry({
      method: 'post',
      endpoint: '/token/generate',
      test: () => {
        expect(resMock.json).to.have.been.calledOnce();
      },
    }));
  });

  describe('handleErrors', () => {
    it('should handle error', async () => {
      // prepare
      const router = new Router(routerArgs);
      const testErr = new Error('testErr');
      const reqMock = new mocks.express.RequestMock();
      const resMock = new mocks.express.ResponseMock();

      // run
      function run() {
        const runner = router.handleErrors(() => { throw testErr; });
        return runner(reqMock, resMock);
      }

      // result
      function test() {
        expect(resMock.json).to.have.been.calledWith({
          result: false,
          errors: {
            code: 500,
            message: testErr.message,
          },
        });
      }

      await run();
      test();
    });
  });

  describe('generateTokenController', () => {
    it('should generate access token', async () => {
      // prepare
      const router = new Router(routerArgs);
      const reqMock = new mocks.express.RequestMock();
      const resMock = new mocks.express.ResponseMock();
      reqMock.body = { test: 'test' };

      // run
      function run() {
        return router.generateTokenController(reqMock, resMock);
      }

      // result
      function test() {
        expect(resMock.json).to.have.been.calledWith({
          result: true,
          data: 'test-token',
        });
      }

      await run();
      test();
    });
  });
});
