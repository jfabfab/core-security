/* global loggerForTests sinon expect */
const proxyquire = require('proxyquire');

describe('Test Suite for Swagger resolver', () => {
  let Swagger;
  let yamljs;
  let $RefParserMock;

  beforeEach(() => {
    yamljs = {
      load: sinon.stub(),
      parse: sinon.stub(),
    };
    // yamljs.load = sinon.stub();

    $RefParserMock = {
      YAML: {
        parse: sinon.stub(),
      },
      dereference: sinon.stub(),
    };

    Swagger = proxyquire('./Swagger', {
      'json-schema-ref-parser': $RefParserMock,
      yamljs,
    }).Swagger;
  });

  it('should resolve path', async () => {
    // prepare
    const topPath = {
      name: 'test-top-path',
      info: {
        $ref: './test-ref',
      },
    };

    const swaggerResolver = new Swagger(loggerForTests);

    yamljs.load.withArgs('test-path').callsFake(() => topPath);
    const filePath = 'test-path';

    // run
    async function run() {
      swaggerResolver.resolveSwaggerFile(filePath);
    }
    // result
    function test() {
      const expectedPathObj = {
        name: 'test-top-path',
        info: {
          $ref: './test-ref',
        },
      };

      expect($RefParserMock.YAML.parse).to.have.been.calledWith(JSON.stringify(expectedPathObj));
      expect($RefParserMock.dereference).to.have.been.called(1);
    }

    await run();
    test();
  });
});
