const assert = require('assert');
const YAML = require('yamljs');
const $RefParser = require('json-schema-ref-parser');

class Swagger {
  constructor(logger) {
    assert(logger, 'logger expected');

    this.logger = logger;
  }

  async resolveSwaggerFile(filePath) {
    try {
      let pathObj;
      pathObj = this._loadYamlPath(filePath);
      pathObj = $RefParser.YAML.parse(JSON.stringify(pathObj));

      const nativeObject = await $RefParser.dereference(pathObj);

      this.logger.log({
        level: 'info',
        message: 'swagger resolved',
      });

      return nativeObject;
    } catch (err) {
      /* istanbul ignore next: global throw err */
      throw err;
    }
  }

  _loadYamlPath(filepath) {
    return YAML.load(filepath);
  }
}

module.exports = {
  Swagger,
};
