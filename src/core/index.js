const { headerVersion } = require('./middlewares/headerVersion');
const { responseFormatter } = require('./middlewares/responseFormatter');
const { Swagger } = require('./resolvers/Swagger');

module.exports = {
  middlewares: {
    headerVersion,
    responseFormatter,
  },
  resolvers: {
    Swagger,
  },
};
