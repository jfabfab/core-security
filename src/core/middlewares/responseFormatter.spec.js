/* global expect sinon */

const { responseFormatter } = require('./responseFormatter');

describe('Test suite for ResponseFormatter', () => {
  describe('format', () => {
    let responseStub;

    beforeEach(() => {
      responseStub = {};
      responseFormatter(null, responseStub, sinon.stub());
    });

    it('should set the error code to 500 by default on error', () => {
      const response = responseStub.formatResponse(new Error());

      expect(response).to.have.nested.property('errors.code', 500);
    });

    it('should set the error code to the provided status', () => {
      const response = responseStub.formatResponse({ status: 404 });

      expect(response).to.have.nested.property('errors.code', 404);
    });

    it('should set the error code to the provided code', () => {
      const response = responseStub.formatResponse({ code: 400 });

      expect(response).to.have.nested.property('errors.code', 400);
    });

    it('should set the error code to the provided httpStatusCode', () => {
      const response = responseStub.formatResponse({ httpStatusCode: 403 });

      expect(response).to.have.nested.property('errors.code', 403);
    });

    it('should set result to false on error', () => {
      const response = responseStub.formatResponse(new Error());

      expect(response).to.have.property('result', false);
    });

    it('should not add the data on error', () => {
      const response = responseStub.formatResponse(new Error(), { test: true });

      expect(response).not.to.have.property('data');
    });

    it('should set result to true when there is no error', () => {
      const response = responseStub.formatResponse(null, {});

      expect(response).to.have.property('result', true);
    });

    it('should add the data to the body', () => {
      const response = responseStub.formatResponse(null, { test: true });

      expect(response).to.have.deep.property('data', { test: true });
    });

    it('should add the error in the body', () => {
      const response = responseStub.formatResponse({ code: 404, message: 'Not Found' });

      expect(response).to.have.nested.property('errors.code', 404);
      expect(response).to.have.nested.property('errors.message', 'Not Found');
    });

    it('should add the error details in the body', () => {
      const response = responseStub.formatResponse({ code: 403, message: 'Forbidden', details: { message: 'Invalid credentials' } });

      expect(response).to.have.nested.property('errors.code', 403);
      expect(response).to.have.nested.property('errors.message', 'Forbidden');
      expect(response.errors).to.have.deep.property('details', { message: 'Invalid credentials' });
    });

    it('should not add the error in the body when there is no error', () => {
      const response = responseStub.formatResponse(null, {});

      expect(response).not.to.have.property('errors');
    });
  });
});
