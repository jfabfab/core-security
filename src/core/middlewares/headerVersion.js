const pkg = require('../../../package.json');

/**
 * Puts the app version in the response
 *
 * @param {any} req - Express request object
 * @param {any} res - Express response object
 * @param {any} next - next function for the middleware
 * @returns
 */
function headerVersion(req, res, next) {
  res.setHeader('X-API-version', pkg.version);
  return next();
}

module.exports = {
  headerVersion,
};
