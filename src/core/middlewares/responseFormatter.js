function format(res, error, data) {
  const response = {
    result: !error,
  };

  if (error) {
    response.errors = {
      code: error.httpStatusCode || error.code || error.status || 500,
      message: error.message ? error.message : 'ERR_INTERNAL_ERROR',
    };
    if (error.details) {
      response.errors.details = error.details;
    }
  } else {
    response.data = data;
  }

  return response;
}


/**
 * Put the formatResponse helper in the res object
 *
 * @param {any} req - Express request object
 * @param {any} res - Express response object
 * @param {any} next - next function for the middleware
 * @returns
 */
function responseFormatter(req, res, next) {
  res.formatResponse = (error, data) => format(res, error, data);

  return next();
}

module.exports = {
  responseFormatter,
};
