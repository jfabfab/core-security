/* global sinon expect */

const { headerVersion } = require('./headerVersion');

describe('Test suite for middlewares.headerVersion', () => {
  let reqMock;
  let resMock;
  let nextMock;

  beforeEach(() => {
    reqMock = {};
    resMock = {
      setHeader: sinon.stub(),
    };
    nextMock = sinon.stub();
  });

  it('should call next middleware', () => {
    // run
    headerVersion(reqMock, resMock, nextMock);

    // result
    expect(nextMock).to.have.been.calledWith();
  });

  it('should set header', () => {
    // run
    headerVersion(reqMock, resMock, nextMock);

    // result
    expect(resMock.setHeader).to.have.been.calledWith('X-API-version');
  });
});
