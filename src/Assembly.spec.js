/* global sinon expect fixtures */

const path = require('path');
const pkg = require('../package.json');
const proxyquire = require('proxyquire');
const configHelper = require('config');

const LoggerMock = {
  error: sinon.stub(),
  info: sinon.stub(),
  log: sinon.stub(),
};

describe('Test suite for Assembly', () => {
  let Assembly;
  let config;
  let initArgs;
  let winston;

  beforeEach(() => {
    config = fixtures.config.clone();
    config.util = configHelper.util;

    winston = sinon.stub();
    winston.createLogger = sinon.stub().returns(LoggerMock);


    const ReporterMock = class {};

    Assembly = proxyquire('./Assembly', {
      '../package.json': fixtures.pkg,
      './reporter/Reporter': { Reporter: ReporterMock },
      winston,
      config,
    }).Assembly;

    initArgs = {
      externalConfigPath: '',
    };
  });

  describe('initialization', () => {
    it('should initialize components', async () => {
      // prepare
      const assembly = new Assembly();

      // run
      async function run() {
        return assembly.init(initArgs);
      }

      // result
      function test() {
        expect(assembly.logger).to.exist();
        expect(assembly.config).to.exist();
        expect(assembly.serviceDriver).to.exist();
        expect(assembly.monitoring).to.exist();
        expect(assembly.router).to.exist();
        expect(assembly.tokenizer).to.exist();
        expect(assembly.reporter).to.exist();
      }

      await run();
      test();
    });

    it('should handle missing private key from config env', async () => {
      // prepare
      config.paths.privateKey = 'test-path';
      const assembly = new Assembly();

      // run
      const promise = assembly.init(initArgs);

      // result
      return expect(promise).to.eventually.be.rejectedWith(/ENOENT/);
    });
  });

  describe('config', () => {
    it('should load external config (dashboard consul config)', async () => {
      // prepare
      const assembly = new Assembly();

      // run
      async function run() {
        return assembly.init({
          externalConfigPath: path.resolve(__dirname, '../test/fixtures/config.console.json'),
        });
      }

      // result
      function test() {
        expect(assembly.config.test_extend).to.equal('test');
        expect(assembly.config.test_extend).to.equal('test');
      }

      await run();
      test();
    });

    it('should detect when external config doesn\'t exist', async () => {
      // prepare
      const assembly = new Assembly();

      // run
      const promise = assembly.init({
        externalConfigPath: '/this_path_should_not_exist/config.console.json',
      });

      // result
      expect(promise).to.eventually.not.be.rejected();
    });

    it('should merge the packagejson into the configs', async () => {
      // prepare
      const assembly = new Assembly();

      const expectedConfig = JSON.parse(JSON.stringify(fixtures.pkg));
      // console.log(JSON.stringify(expectedConfig));
      delete expectedConfig.dependencies;
      delete expectedConfig.devDependencies;
      delete expectedConfig.scripts;

      // run
      async function run() {
        return assembly.init(initArgs);
      }

      // result
      function test() {
        expect(assembly.config.service).to.deep.include(expectedConfig);
      }

      await run();
      test();
    });

    it('should put the full service url in the configs based on the host property', async () => {
      // prepare
      const assembly = new Assembly();

      // run
      async function run() {
        return assembly.init(initArgs);
      }

      // result
      function test() {
        expect(assembly.config.service.url).to.deep.equal(`https://host/${pkg.coreSecurity.serviceGroup}/v1/test-service`);
      }

      await run();
      test();
    });

    it('should put the full service url in the configs based on cie/host', async () => {
      // prepare
      config.apigateway.host = '';
      const assembly = new Assembly();

      // run
      async function run() {
        return assembly.init(initArgs);
      }

      // result
      function test() {
        expect(assembly.config.service.url).to.deep.equal(`https://host_prefix/${pkg.coreSecurity.serviceGroup}/v1/test-service`);
      }

      await run();
      test();
    });

    it('should set the reporter endpoint to default when apigateway route is not provided', async () => {
      // prepare
      config.routes = {};
      const assembly = new Assembly();

      // run
      async function run() {
        return assembly.init(initArgs);
      }

      // result
      function test() {
        expect(assembly.reporter.endpoint).to.deep.equal('');
      }

      await run();
      test();
    });

    it('should set the reporter endpoint to the config route apigateway', async () => {
      // prepare
      config.routes = {
        apigateway: 'test-endpoint',
      };
      const assembly = new Assembly();

      // run
      async function run() {
        return assembly.init(initArgs);
      }

      // result
      function test() {
        expect(assembly.reporter.endpoint).to.deep.equal('test-endpoint/issues');
      }

      await run();
      test();
    });
  });

  describe('clean method', () => {
    it('should clean', async () => {
      // prepare
      const assembly = new Assembly();

      async function prepare() {
        return assembly.init(initArgs);
      }

      // run
      function run() {
        assembly.clean();
      }

      // result
      function test() {
        // TODO: Add your clean tests here
      }

      await prepare();
      run();
      test();
    });

    it('should clean even when init is not called', async () => {
      // prepare
      const assembly = new Assembly();

      // run
      function run() {
        assembly.clean();
      }

      // result

      expect(() => run()).to.not.throw();
    });
  });
});
