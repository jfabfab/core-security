/* global expect sinon loggerForTests */
const { Tokenizer } = require('./Tokenizer');

describe('Test Suite for Tokenizer', () => {
  describe('Class Initialisation', () => {
    function entry(desc, {
      expectedErr,
      params,
    }) {
      it(desc, () => {
        expect(() => new Tokenizer(params)).to.throw(expectedErr);
      });
    }

    entry('Should handle missing logger', {
      expectedErr: /expects logger/,
      params: {},
    });

    entry('Should handle missing jsonwebtoken', {
      expectedErr: /expects jsonWebToken/,
      params: {
        logger: loggerForTests,
      },
    });
  });

  describe('generate method', () => {
    it('Should handle errors while encoding token', async () => {
      // prepare
      const errorMock = new Error('encode-error');
      const tokenizer = new Tokenizer({
        logger: loggerForTests,
        jsonWebToken: {
          encode: sinon.stub().throws(errorMock),
        },
      });

      // test
      try {
        const args = {};
        await tokenizer.generate(args);
      } catch (e) {
        expect(e).to.equal(errorMock);
      }
    });

    it('Should return generated token', async () => {
      // prepare
      const jsonWebTokenMock = {
        encode: sinon.stub().returns('jwt-token'),
      };

      const tokenizer = new Tokenizer({
        logger: loggerForTests,
        jsonWebToken: jsonWebTokenMock,
      });

      // run
      async function run() {
        const tokenData = {
          userid: '123',
          name: 'name',
          isAdmin: false,
        };
        return tokenizer.generate(tokenData);
      }

      // test
      function test(token) {
        const expectedArgs = {
          payload: {
            name: 'name',
            isAdmin: false,
          },
          userid: '123',
        };

        expect(jsonWebTokenMock.encode.calledOnce).to.equal(true);
        expect(jsonWebTokenMock.encode).to.have.been.calledWith(expectedArgs);
        expect(token).to.equal('jwt-token');
      }

      const token = await run();
      test(token);
    });
  });
});
