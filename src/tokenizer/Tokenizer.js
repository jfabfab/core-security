const assert = require('assert');
const { EventEmitter } = require('events');

class Tokenizer extends EventEmitter {
  constructor({
    logger,
    jsonWebToken,
  }) {
    super();
    assert(logger, 'Tokenizer Class expects logger');
    assert(jsonWebToken, 'Tokenizer Class expects jsonWebToken');

    this.logger = logger;
    this.jsonWebToken = jsonWebToken;
  }

  /**
   * Generates signed access token
   *
   * @param {*} { userid, name, isAdmin }
   * @returns {*} JWTtoken
   */
  async generate({
    userid,
    name,
    isAdmin,
  }) {
    try {
      const token = await this.jsonWebToken.encode({
        payload: {
          name,
          isAdmin,
        },
        userid,
      });

      this.emit('SIGNED');

      return token;
    } catch (e) {
      throw (e);
    }
  }
}

module.exports = {
  Tokenizer,
};
