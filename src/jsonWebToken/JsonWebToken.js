const jwt = require('jsonwebtoken');
const assert = require('assert');
const util = require('util');

const promisifiedJWTSign = util.promisify(jwt.sign);

class JsonWebToken {
  constructor({
    privateKey,
  }) {
    assert(privateKey, 'JsonWebToken expects privateKey');

    this.privateKey = privateKey;
  }

  /**
   * encode
   * @param {any} params
   * @returns {string} token
   */
  async encode({ userid, payload }) {
    try {
      const token = await promisifiedJWTSign(payload, this.privateKey, {
        algorithm: 'RS256',
        expiresIn: '2d',
        subject: userid,
      });

      return token;
    } catch (err) {
      throw err;
    }
  }
}

module.exports = {
  JsonWebToken,
};
