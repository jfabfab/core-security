/* global expect sinon */

const proxyquire = require('proxyquire');
const { JsonWebToken } = require('./JsonWebToken');

describe('Test Suite for JsonWebToken class', () => {
  describe('Initialization', () => {
    function entry(desc, {
      expectedErr,
      params,
    }) {
      it(desc, () => {
        expect(() => new JsonWebToken(params)).to.throw(expectedErr);
      });
    }

    entry('Should handle missing privateKey', {
      expectedErr: /expects privateKey/,
      params: {},
    });
  });

  describe('sign method', () => {
    let jsonwebtokenMock;
    let JWT;

    beforeEach(() => {
      jsonwebtokenMock = {
        sign: sinon.stub(),
      };

      jsonwebtokenMock.sign.callsFake(() => 'test-encoded-token');

      const utilMock = {
        promisify: sinon.stub().callsFake(fn => fn),
      };

      JWT = proxyquire('./JsonWebToken', {
        jsonwebtoken: jsonwebtokenMock,
        util: utilMock,
      }).JsonWebToken;
    });

    it('Should handle errors from jsonwebtoken sign method', () => {
      // prepare
      const errorMock = new Error('jwt-sign-error');
      const payloadMock = {};
      jsonwebtokenMock.sign.throws(errorMock);

      const jsonWebToken = new JWT({
        privateKey: 'test-key',
      });

      // results
      const promise = jsonWebToken.encode({
        userid: 'test-user-id',
        payload: payloadMock,
      });

      return expect(promise).to.be.eventually.rejectedWith(errorMock);
    });


    it('Should use jsonwebtoken sign method', async () => {
      // prepare
      const payloadMock = {};

      const jsonWebToken = new JWT({
        privateKey: 'test-key',
      });

      // run
      async function run() {
        return jsonWebToken.encode({
          userid: 'test-userid',
          payload: payloadMock,
        });
      }

      // results
      function test(token) {
        const expectedToken = 'test-encoded-token';
        expect(token).to.equal(expectedToken);
      }

      const token = await run();
      test(token);
    });
  });
});
