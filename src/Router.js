const assert = require('assert');
const bodyParser = require('body-parser');
const core = require('./core');
const cors = require('cors');
// const favicon = require('serve-favicon');
// const path = require('path');
const pkg = require('../package.json');

class Router {
  constructor({
    logger,
    config,
    monitoring,
    serviceDriver,
    swaggerConfig,
    swaggerMiddleware,
    tokenizer,
    reporter,
  }) {
    assert(logger, 'expected logger');
    assert(config, 'expected config');
    assert(monitoring, 'expected monitoring');
    assert(serviceDriver, 'expected serviceDriver');
    assert(swaggerConfig, 'expected swaggerConfig');
    assert(swaggerMiddleware, 'expected swaggerMiddleware');
    assert(tokenizer, 'expected tokenizer');
    assert(reporter, 'expected reporter');

    this.logger = logger;
    this.config = config;
    this.monitoring = monitoring;
    this.serviceDriver = serviceDriver; // expressjs
    this.swaggerConfig = swaggerConfig;
    this.swaggerMiddleware = swaggerMiddleware;
    this.tokenizer = tokenizer;
    this.reporter = reporter;
  }

  registerRoutes() {
    // global middleware
    // this.serviceDriver.use(favicon(path.join(__dirname, 'favicon.ico')));
    this.serviceDriver.use(core.middlewares.headerVersion); // add version in header of every res
    this.serviceDriver.use(core.middlewares.responseFormatter); // add format method in every res
    this.serviceDriver.use(bodyParser.json()); // for parsing application/json
    this.serviceDriver.use(
      bodyParser.urlencoded({ extended: true }) // application/x-www-form-urlencoded
    );

    // global routes
    this.serviceDriver.get('/health', cors(), this.handleErrors(this.healthController));
    this.serviceDriver.use('/swagger.yaml', cors(), (req, res) => this.swaggerController(req, res));

    this.serviceDriver.get('/', (req, res) => this.homeController(req, res));

    // validation middleware (validate only on APIs and not global routes)
    this.serviceDriver.use(
      this.swaggerMiddleware.metadata(),
      this.swaggerMiddleware.CORS(),
      this.swaggerMiddleware.parseRequest(),
      this.swaggerMiddleware.validateRequest()
    );

    this.serviceDriver.post('/token/generate', this.handleErrors(this.generateTokenController));

    // error handler, should be registered last
    this.serviceDriver.use(this.errorMiddleware.bind(this));
  }

  /**
   * GET: /home
   *
   * @param {any} req
   * @param {any} res
   * @memberof Router
   */
  homeController(req, res) {
    res.send(`<html><body>${pkg.name} v${pkg.version}</body></html>`);
  }

  /**
   * GET: /health
   *
   * @param {any} req
   * @param {any} res
   * @memberof Router
   */
  healthController(req, res) {
    const healthResult = {
      status: 200,
      monitoring: this.monitoring.getCurrentCounters(),
      // NOTE: add your test for mongodb, elasticsearch...
      // https://itwiki.coreSecurity.com/display/DT/Monitoring
    };

    res.json(healthResult);
  }

  /**
   * Handles swagger yaml
   *
   * @param {any} req
   * @param {any} res
   *
   * @memberof Router
   */
  swaggerController(req, res) {
    res
      .status(200)
      .type('yaml')
      .send(this.swaggerConfig);
  }

  /**
   * Handle errors on all controllers
   *
   * @param {any} err
   * @param {any} res
   * @param {any} req
   * @param {any} next
   *
   * @memberof Router
   */
  errorMiddleware(err, req, res, /* eslint-disable */ next) {
    this.logger.error(JSON.stringify(err));
    res.json(res.formatResponse(err, null));
  }

  /**
   * Wraps the controller in an error handling code
   * This allows your controller to return a promise and errors will be handled by the error middleware
   *
   * @param {any} controller
   * @returns
   * @memberof Router
   */
  handleErrors(controller) {
    const self = this;
    return async function(req, res) {
      try {
        await controller.bind(self)(req, res);
      } catch (err) {
        self.errorMiddleware(err, req, res);
        const result = await self.reporter.reportIssue(err);
        self.logger.info(result);
      }
    };
  }

  /**
   * POST: /authenticate
   *
   * @param {any} req
   * @param {any} res
   * @memberof Router
   */
  async generateTokenController(req, res) {
    const results = await this.tokenizer.generate(req.body);
    res.json(res.formatResponse(null, results));
  }
}

module.exports = {
  Router,
};
