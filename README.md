# core-security
This is a service to provide access tokens and other means of validation for authenticated users. It is part of the core services that can be integrated in all microservices based projects.
