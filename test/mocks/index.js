const { AppMock, RequestMock, ResponseMock } = require('./express.mock');
const { MiddlewareMock } = require('./swagger.mock');

module.exports = {
  express: {
    AppMock,
    RequestMock,
    ResponseMock,
  },
  swagger: {
    MiddlewareMock,
  },
};
