# Scripts

This is where you can put build scripts or helper scripts.
You can then register npm scripts to run the scripts here

```json
"scripts": {
  "start": "node .",
  "myJsScript": "node ./scripts/custom.js",
  "myBashScript": "./scipts/custom.sh",
  ...
},
```
